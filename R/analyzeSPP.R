#' Perfrom full SPP analysis
#'
#' @param df data frame of SPP profiles including the
#' columns representative, temperature, log.conc, value
#' and window (if window should be used as a grouping variable)
#' @param ncores numeric value indicating the number of
#' cores that should be used to parallelize bootstrap runs
#' forFDR computation
#' @param bootstraps numeric value indicating the number of
#' bootstraps that should be run to estimate FDR, default is
#' set to 100
#'
#' @return data frame
#'
#' @examples
#' data("spp_example_df")
#' temp_df <- filter(spp_example_df,
#'                   name %in% paste0("protein", 1:3))
#' analyzeSPP(spp_example_df,
#'            bootstraps = 2)
#' @export
#'
#' @import dplyr
analyzeSPP <- function(df, ncores = 1,
                       bootstraps = 100){

  rel_value <- F.statistic <- NULL

  h0stats <- fitH0ModelSPP(df)
  h1stats <- fitH1ModelSPP(df)
  sum.df <- summarizeFstatSPP(h0stats, h1stats)

  registerDoParallel(cores = ncores)

  set.seed(12, kind = "L'Ecuyer-CMRG")
  res.list <- foreach(boot=seq(bootstraps)) %dopar% {
    perm.df <- df %>%
      mutate(rel_value = sample(rel_value, replace = TRUE))

    d.h0stats <- fitH0ModelSPP(perm.df)
    d.h1stats <- fitH1ModelSPP(perm.df)
    d.sum.df <- summarizeFstatSPP(d.h0stats, d.h1stats)

    h0h1.sum.df <- rbind(sum.df %>% mutate(dataset = "target"),
                         d.sum.df %>% mutate(dataset = "decoy"))

    res.df <- h0h1.sum.df %>% group_by() %>%
      arrange(desc(F.statistic)) %>%
      mutate(rank = dense_rank(desc(F.statistic)),
             cum.f = F.statistic,
             max.fc = 1)

    return(res.df)
  }

  fdr.df <- computeAverageFdr(res.list, fc.thres = 1)
  return(fdr.df)
}
