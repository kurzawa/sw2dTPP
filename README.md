# FDR-controlled analysis of 2D-TPP experiments using functional modeling of sliding temperature windows

This package contains functions to analyze 2D-thermal proteome profiles and was specifically implemented for such experiments with the goal of detecting protein-metabolite interations. It was used in Sridharan et al. (2018) bioRxiv [https://doi.org/10.1101/488163](https://doi.org/10.1101/488163) to infer ATP- and GTP-protein interactions from 2D-TPP data.

## Installation

```{R}
require("devtools")
devtools::install_git("https://git.embl.de/kurzawa/sw2dTPP.git")
```
